# Mail

Mail is an open-source email client for Android.  
Mail is forked from [k9 Mail](https://github.com/k9mail/k-9)

## Authors

[Authors](https://gitlab.e.foundation/e/os/Mail/-/blob/master/AUTHORS)

## Release Notes

Check out the [Release Notes](https://gitlab.e.foundation/e/os/Mail/-/releases) to find out what changed
in each version of Mail.

## Privacy Policy

[Privacy Policy](https://e.foundation/legal-notice-privacy)  
[Terms of service](https://e.foundation/legal-notice-privacy)

## License

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
