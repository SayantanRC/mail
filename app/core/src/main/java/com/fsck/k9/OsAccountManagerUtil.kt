/*
 * Copyright MURENA SAS 2022
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.fsck.k9

import android.accounts.Account
import android.accounts.AccountManager
import android.content.ContentResolver
import android.content.Context
import app.k9mail.core.android.common.accountmanager.AccountManagerConstants
import timber.log.Timber

object OsAccountManagerUtil {

    /**
     * @return syncEnable or not. If account not found in accountManager accounts,
     * means account is setup by user... so return true by default
     */
    fun isSyncEnable(context: Context, email: String): Boolean {
        val accountManager = AccountManager.get(context)

        var syncStatus = true

        AccountManagerConstants.ALL_ACCOUNT_TYPES.forEach {
            val accounts = accountManager.getAccountsByType(it)
            val syncEnable = isSyncEnable(accountManager, accounts, email)
            if (syncEnable.isAccountFound()) {
                syncStatus = syncEnable.getStatus()
                return@forEach
            }
        }

        return syncStatus
    }

    private fun isSyncEnable(accountManager: AccountManager, accounts: Array<out Account>, email: String): Syncable {
        accounts.forEach {
            try {
                val accountEmail: String =
                    accountManager.getUserData(it, AccountManagerConstants.ACCOUNT_EMAIL_ADDRESS_KEY)
                if (accountEmail == email) {
                    // if master sync disable, then account sync is disable
                    return if (!ContentResolver.getMasterSyncAutomatically()) {
                        Syncable.NOT_SYNCABLE
                    } else Syncable.getSyncable(
                        ContentResolver.getSyncAutomatically(
                            it,
                            AccountManagerConstants.MAIL_CONTENT_AUTHORITY,
                        ),
                    )
                }
            } catch (e: Throwable) {
                Timber.e(e)
            }
        }

        return Syncable.ACCOUNT_NOT_FOUND
    }

    private enum class Syncable {
        SYNCABLE,
        NOT_SYNCABLE,
        ACCOUNT_NOT_FOUND;

        companion object {
            fun getSyncable(status: Boolean) = if (status) SYNCABLE else NOT_SYNCABLE
        }

        fun getStatus() = this == SYNCABLE

        fun isAccountFound() = this != ACCOUNT_NOT_FOUND
    }
}
