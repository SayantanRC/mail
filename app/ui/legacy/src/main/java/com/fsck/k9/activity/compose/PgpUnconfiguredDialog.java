/*
 * Copyright ECORP SAS 2022
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.fsck.k9.activity.compose;


import android.annotation.SuppressLint;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;

import androidx.annotation.IdRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.fsck.k9.ui.R;
import com.fsck.k9.view.HighlightDialogFragment;


public class PgpUnconfiguredDialog extends HighlightDialogFragment {

    public static PgpUnconfiguredDialog newInstance(@IdRes int showcaseView) {
        final PgpUnconfiguredDialog dialog = new PgpUnconfiguredDialog();

        final Bundle args = new Bundle();
        args.putInt(ARG_HIGHLIGHT_VIEW, showcaseView);
        dialog.setArguments(args);

        return dialog;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        @SuppressLint("InflateParams")
        final View view = LayoutInflater.from(getActivity()).inflate(R.layout.openpgp_enable_encryption_dialog, null);

        final Builder builder = new Builder(getActivity())
                .setView(view);

        builder.setPositiveButton(R.string.openpgp_dialog_proceed, (dialog, which) -> {
            if (!(getActivity() instanceof OpenPgpConfigureCallBack)) {
                return;
            }

            ((OpenPgpConfigureCallBack) getActivity()).initializePgpSetup();
            dialog.dismiss();
        });

        builder.setNegativeButton(R.string.openpgp_dialog_cancel, (dialog, which) -> dialog.dismiss());

        return builder.create();
    }
}
