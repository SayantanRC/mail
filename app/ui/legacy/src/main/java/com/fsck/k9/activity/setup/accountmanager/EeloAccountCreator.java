/*
 * Copyright ECORP SAS 2022
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.fsck.k9.activity.setup.accountmanager;


import java.util.List;

import android.accounts.AccountManager;
import android.content.ContentResolver;
import android.content.Context;
import android.os.Build.VERSION_CODES;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.annotation.WorkerThread;
import app.k9mail.autodiscovery.api.DiscoveredServerSettings;
import app.k9mail.autodiscovery.api.DiscoveryResults;
import app.k9mail.autodiscovery.providersxml.ProvidersXmlDiscovery;
import app.k9mail.core.android.common.accountmanager.AccountManagerConstants;
import app.k9mail.core.common.mail.Protocols;
import com.fsck.k9.Account;
import com.fsck.k9.Account.DeletePolicy;
import com.fsck.k9.Core;
import com.fsck.k9.DI;
import com.fsck.k9.Preferences;
import com.fsck.k9.account.AccountCreatorHelper;
import com.fsck.k9.account.BackgroundAccountRemover;
import com.fsck.k9.job.K9JobManager;
import com.fsck.k9.mail.AuthType;
import com.fsck.k9.mail.ConnectionSecurity;
import com.fsck.k9.mail.ServerSettings;
import com.fsck.k9.mailstore.SpecialLocalFoldersCreator;
import com.fsck.k9.ui.ConnectionSettings;
import timber.log.Timber;


public class EeloAccountCreator {
    private static final ProvidersXmlDiscovery providersXmlDiscovery = DI.get(ProvidersXmlDiscovery.class);
    private static final AccountCreatorHelper accountCreatorHelper = DI.get(AccountCreatorHelper.class);
    private static final SpecialLocalFoldersCreator localFoldersCreator = DI.get(SpecialLocalFoldersCreator.class);

    @RequiresApi(api = VERSION_CODES.N)
    @WorkerThread
    public static void loadAccountsFromAccountManager(@NonNull Context context, @NonNull Preferences preferences,
        @NonNull BackgroundAccountRemover accountRemover, @NonNull K9JobManager jobManager,
        @Nullable OnAccountLoadCompleteCallBack callBack) {
        try {
            AccountManager accountManager = AccountManager.get(context);

            List<Account> accounts = preferences.getAccounts();
            deleteIncompleteAccounts(accounts, accountRemover);

            loadEeloAccounts(context, accounts, accountManager, jobManager);

            AccountManagerConstants.INSTANCE.getOPENID_ACCOUNT_TYPES()
                .forEach(accountType -> loadOpenIdAccounts(context, accountType, accounts, accountManager, jobManager));
        } catch (SecurityException e) {
            Timber.e(e, "Failed to load accounts from accountManager because of security violation");
        }
        if (callBack != null) {
            callBack.onAccountLoadComplete();
        }
    }

    // check the account sync for mail authority is enable or not
    private static boolean isSyncable(@Nullable android.accounts.Account account) {
        if (account == null) {
            return false;
        }

        // if master sync disable, then account sync is disable
        if (!ContentResolver.getMasterSyncAutomatically()) {
            return false;
        }

        return ContentResolver.getSyncAutomatically(account, AccountManagerConstants.MAIL_CONTENT_AUTHORITY);
    }

    @RequiresApi(api = VERSION_CODES.N)
    private static void loadOpenIdAccounts(@NonNull Context context, @NonNull String accountType,
        List<Account> accounts,
        @NonNull AccountManager accountManager, @NonNull K9JobManager jobManager) {
        android.accounts.Account[] openIdAccounts =
            accountManager.getAccountsByType(accountType);
        for (android.accounts.Account openIdAccount : openIdAccounts) {
            String emailId =
                accountManager.getUserData(openIdAccount, AccountManagerConstants.ACCOUNT_EMAIL_ADDRESS_KEY);
            if (isInvalidEmail(emailId)) {
                continue;
            }

            if (!isSyncable(openIdAccount)) {
                continue;
            }

            if (!isOpenIdAccount(accountManager, openIdAccount)) {
                continue;
            }

            var existenceAccount = accounts.stream()
                .filter(account -> emailId.equalsIgnoreCase(account.getEmail()))
                .peek(account -> updateAccountNameIfMissing(context, emailId, account))
                .findAny();

            if (!existenceAccount.isPresent()) {
                String authState = accountManager.getUserData(openIdAccount, AccountManagerConstants.KEY_AUTH_STATE);
                createAccount(context, emailId, "", authState, accountType);
                continue;
            }

            jobManager.scheduleAllMailJobs();
        }
    }

    private static boolean isOpenIdAccount(AccountManager accountManager, android.accounts.Account account) {
        final String authState = accountManager.getUserData(account, AccountManagerConstants.KEY_AUTH_STATE);
        return authState != null && !authState.trim().isEmpty();
    }

    private static void updateAccountNameIfMissing(@NonNull Context context, String emailId, Account account) {
        if (account.getName() == null) { // we need to fix an old bug
            account.setName(emailId);
            Preferences.getPreferences().saveAccount(account);
        }
    }

    private static boolean isInvalidEmail(String emailId) {
        return emailId == null || !emailId.contains("@");
    }

    @RequiresApi(api = VERSION_CODES.N)
    private static void loadEeloAccounts(@NonNull Context context, List<Account> accounts,
        @NonNull AccountManager accountManager, @NonNull K9JobManager jobManager) {
        android.accounts.Account[] eeloAccounts =
            accountManager.getAccountsByType(AccountManagerConstants.EELO_ACCOUNT_TYPE);
        for (android.accounts.Account eeloAccount : eeloAccounts) {
            String emailId = accountManager.getUserData(eeloAccount, AccountManagerConstants.ACCOUNT_EMAIL_ADDRESS_KEY);
            if (isInvalidEmail(emailId)) {
                continue;
            }

            if (!isSyncable(eeloAccount)) {
                continue;
            }

            if (isOpenIdAccount(accountManager, eeloAccount)) {
                continue;
            }

            var existenceAccount = accounts.stream()
                .filter(account -> emailId.equalsIgnoreCase(account.getEmail()))
                .findAny();

            if (!existenceAccount.isPresent()) {
                String password = accountManager.getPassword(eeloAccount);
                createAccount(context, emailId, password, null, AccountManagerConstants.EELO_ACCOUNT_TYPE);
                continue;
            }

            jobManager.scheduleAllMailJobs();
        }
    }

    @RequiresApi(api = VERSION_CODES.N)
    private static void deleteIncompleteAccounts(List<Account> accounts, BackgroundAccountRemover accountRemover) {
        accounts.stream().filter(account -> !account.isFinishedSetup())
            .forEach(account -> accountRemover.removeAccountAsync(account.getUuid()));
    }

    private static void createAccount(Context context, String emailId, String password, @Nullable String authState,
        @NonNull String accountType) {
        Preferences preferences = Preferences.getPreferences();

        Account account = preferences.newAccount();
        account.setChipColor(accountCreatorHelper.pickColor());
        account.setEmail(emailId);
        account.setName(emailId);
        account.setSenderName(emailId);

        ConnectionSettings connectionSettings = providersXmlDiscoveryDiscover(emailId);
        if (connectionSettings == null) {
            // connection details not predefined in the xml. Try to load from the api
            connectionSettings = MailAutoConfigDiscovery.retrieveConfigFromApi(emailId);
        }
        // providers.xml doesn't have the connection details & can't retrieve details from api
        // In this case, fall back to default configuration.
        if (connectionSettings == null && authState != null) {
            connectionSettings = providersDefaultOpenIdAccountDiscover(emailId, accountType);
        }

        if (connectionSettings == null) {
            Timber.e("Error while trying to initialise account configuration.");
            return;
        }
        ServerSettings incomingSettings = connectionSettings.getIncoming().newPassword(password);
        ServerSettings outgoingSettings = connectionSettings.getOutgoing().newPassword(password);

        if (authState != null && !authState.trim().isEmpty()) {
            incomingSettings = incomingSettings.newAuthenticationType(AuthType.XOAUTH2);
            outgoingSettings = outgoingSettings.newAuthenticationType(AuthType.XOAUTH2);
        }

        account.setIncomingServerSettings(incomingSettings);
        account.setOutgoingServerSettings(outgoingSettings);

        account.setOAuthState(authState);

        DeletePolicy deletePolicy = accountCreatorHelper.getDefaultDeletePolicy(incomingSettings.type);
        account.setDeletePolicy(deletePolicy);

        localFoldersCreator.createSpecialLocalFolders(account);
        account.markSetupFinished();

        preferences.saveAccount(account);
        Core.setServicesEnabled(context);
    }

    private static ConnectionSettings providersXmlDiscoveryDiscover(String email) {
        DiscoveryResults discoveryResults =
            providersXmlDiscovery.discover(email);
        if (discoveryResults == null ||
            (discoveryResults.getIncoming().size() < 1 || discoveryResults.getOutgoing().size() < 1)) {
            return null;
        }
        DiscoveredServerSettings incoming = discoveryResults.getIncoming().get(0);
        DiscoveredServerSettings outgoing = discoveryResults.getOutgoing().get(0);
        return new ConnectionSettings(
            new ServerSettings(
                incoming.getProtocol(),
                incoming.getHost(),
                incoming.getPort(),
                incoming.getSecurity(),
                incoming.getAuthType(),
                incoming.getUsername(),
                null,
                null
            ),
            new ServerSettings(
                outgoing.getProtocol(),
                outgoing.getHost(),
                outgoing.getPort(),
                outgoing.getSecurity(),
                outgoing.getAuthType(),
                outgoing.getUsername(),
                null,
                null
            )
        );
    }

    private static ConnectionSettings providersDefaultOpenIdAccountDiscover(@NonNull String email,
        @NonNull String accountType) {
        String incomingHost = "mail.ecloud.global";
        String outgoingHost = incomingHost;
        int outgoingPort = 587;
        ConnectionSecurity outgoingConnectionSecurity = ConnectionSecurity.STARTTLS_REQUIRED;

        if (accountType.equals(AccountManagerConstants.GOOGLE_ACCOUNT_TYPE)) {
            incomingHost = "imap.gmail.com";
            outgoingHost = "smtp.gmail.com";
            outgoingPort = 465;
            outgoingConnectionSecurity = ConnectionSecurity.SSL_TLS_REQUIRED;
        } else if (accountType.equals(AccountManagerConstants.YAHOO_ACCOUNT_TYPE)) {
            incomingHost = "imap.mail.yahoo.com";
            outgoingHost = "smtp.mail.yahoo.com";
            outgoingPort = 465;
            outgoingConnectionSecurity = ConnectionSecurity.SSL_TLS_REQUIRED;
        }

        return new ConnectionSettings(
            new ServerSettings(
                Protocols.IMAP,
                incomingHost,
                993,
                ConnectionSecurity.SSL_TLS_REQUIRED,
                AuthType.XOAUTH2,
                email,
                null,
                null
            ),
            new ServerSettings(
                Protocols.SMTP,
                outgoingHost,
                outgoingPort,
                outgoingConnectionSecurity,
                AuthType.XOAUTH2,
                email,
                null,
                null
            )
        );
    }

    public interface OnAccountLoadCompleteCallBack {
        void onAccountLoadComplete();
    }
}

