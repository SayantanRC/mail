package com.fsck.k9.activity.setup.accountmanager;


import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Url;


/**
 * To retrieve /e/ cloud mail configuration settings from api
 */
public interface EeloMailAutoConfigApi {

    @GET
    Call<EeloMailAutoConfigResponse> getConfig(@Url String url);
}
