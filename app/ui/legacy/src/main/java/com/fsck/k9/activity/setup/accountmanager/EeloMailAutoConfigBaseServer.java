package com.fsck.k9.activity.setup.accountmanager;


import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;


public abstract class EeloMailAutoConfigBaseServer {

    @Attribute
    private String type;

    @Element
    private String hostname;

    @Element(name = "port", type = Integer.class)
    private int port;

    @Element
    private String socketType;

    @Element
    private String authentication;

    @Element
    private String username;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getHostname() {
        return hostname;
    }

    public void setHostname(String hostname) {
        this.hostname = hostname;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public String getSocketType() {
        return socketType;
    }

    public void setSocketType(String socketType) {
        this.socketType = socketType;
    }

    public String getAuthentication() {
        return authentication;
    }

    public void setAuthentication(String authentication) {
        this.authentication = authentication;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
