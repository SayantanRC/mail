/*
 * Copyright ECORP SAS 2022
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.fsck.k9.activity.setup.accountmanager;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

@Root(name = "emailProvider", strict = false)
public class EeloMailAutoConfigEmailProvider {

    @Attribute
    private String id;

    @Element
    private EeloMailAutoConfigOutgoingServer outgoingServer;

    @Element
    private EeloMailAutoConfigIncomingServer incomingServer;

    public EeloMailAutoConfigEmailProvider() {
    }

    public EeloMailAutoConfigEmailProvider(String id,
            EeloMailAutoConfigOutgoingServer outgoingServer,
            EeloMailAutoConfigIncomingServer incomingServer) {
        this.id = id;
        this.outgoingServer = outgoingServer;
        this.incomingServer = incomingServer;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public EeloMailAutoConfigOutgoingServer getOutgoingServer() {
        return outgoingServer;
    }

    public void setOutgoingServer(EeloMailAutoConfigOutgoingServer outgoingServer) {
        this.outgoingServer = outgoingServer;
    }

    public EeloMailAutoConfigIncomingServer getIncomingServer() {
        return incomingServer;
    }

    public void setIncomingServer(EeloMailAutoConfigIncomingServer incomingServer) {
        this.incomingServer = incomingServer;
    }
}
