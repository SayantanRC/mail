package com.fsck.k9.activity.setup.accountmanager;


import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;


@Root(name = "clientConfig", strict = false)
public class EeloMailAutoConfigResponse {

    @Attribute
    private String version;

    @Element
    private EeloMailAutoConfigEmailProvider emailProvider;

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public EeloMailAutoConfigEmailProvider getEmailProvider() {
        return emailProvider;
    }

    public void setEmailProvider(EeloMailAutoConfigEmailProvider emailProvider) {
        this.emailProvider = emailProvider;
    }
}
