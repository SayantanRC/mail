/*
 * Copyright ECORP SAS 2022
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.fsck.k9.activity.setup.accountmanager;


import androidx.annotation.Nullable;
import app.k9mail.autodiscovery.api.DiscoveredServerSettings;
import com.fsck.k9.helper.EmailHelper;
import com.fsck.k9.helper.Utility;
import com.fsck.k9.mail.AuthType;
import com.fsck.k9.mail.ConnectionSecurity;
import com.fsck.k9.mail.ServerSettings;
import com.fsck.k9.ui.ConnectionSettings;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.simplexml.SimpleXmlConverterFactory;
import timber.log.Timber;

public class MailAutoConfigDiscovery {

    /**
     * Try to retrieve mail info from the api
     *
     * @return mail's incoming & outgoing connection detail, null if failed to retrieve
     */
    @Nullable
    public static ConnectionSettings retrieveConfigFromApi(String email) {
        String domain = EmailHelper.getDomainFromEmailAddress(email);
        if (domain == null) {
            return null;
        }

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(
                        "https://autoconfig.e.email/") //dummy url. Retrofit required base url to configure. But we will use @Url on th api interface level
                .addConverterFactory(SimpleXmlConverterFactory.create())
                .build();

        EeloMailAutoConfigApi eeloMailAutoConfigApi = retrofit.create(EeloMailAutoConfigApi.class);
        // api's basic url pattern is: https://autoconfig.${DOMAIN}/mail/config-v1.1.xml?emailaddress=${USER}@${DOMAIN}
        Call<EeloMailAutoConfigResponse>
                eeloMailAutoConfigResponseCall = eeloMailAutoConfigApi
                .getConfig("https://autoconfig." + domain + "/mail/config-v1.1.xml?emailaddress=" + email);
        try {
            Response<EeloMailAutoConfigResponse> response = eeloMailAutoConfigResponseCall.execute();
            if (response.isSuccessful()) {
                EeloMailAutoConfigResponse eeloMailAutoConfigResponse = response.body();
                return providersAutoConfigDiscoveryDiscover(eeloMailAutoConfigResponse);
            }
        } catch (Exception e) {
            Timber.e(e);
        }
        return null;
    }

    /**
     * Parse connection settings from api response
     */
    private static ConnectionSettings providersAutoConfigDiscoveryDiscover(EeloMailAutoConfigResponse response) {
        if (response == null) {
            return null;
        }

        DiscoveredServerSettings incoming = getAutoConfigIncomingSettings(response);
        DiscoveredServerSettings outgoing = getAutoConfigOutgoingSettings(response);

        if (incoming == null || outgoing == null) {
            return null;
        }

        return new ConnectionSettings(
                new ServerSettings(
                        incoming.getProtocol(),
                        incoming.getHost(),
                        incoming.getPort(),
                        incoming.getSecurity(),
                        incoming.getAuthType(),
                        incoming.getUsername(),
                        null,
                        null
                ),
                new ServerSettings(
                        outgoing.getProtocol(),
                        outgoing.getHost(),
                        outgoing.getPort(),
                        outgoing.getSecurity(),
                        outgoing.getAuthType(),
                        outgoing.getUsername(),
                        null,
                        null
                )
        );
    }

    /**
     * parse common settings for incoming & outgoing mail server
     */
    private static DiscoveredServerSettings getAutoConfigSettings(EeloMailAutoConfigBaseServer server) {
        if (Utility.isStringEmpty(server.getType())) {
            return null;
        }
        if (Utility.isStringEmpty(server.getHostname())) {
            return null;
        }

        if (Utility.isStringEmpty(server.getUsername())) {
            return null;
        }

        AuthType authType = getAuthType(server.getAuthentication());
        if (authType == null) {
            return null;
        }

        if (Utility.isStringEmpty(server.getSocketType())) {
            return null;
        }

        ConnectionSecurity security = getConnectionSecurity(server.getSocketType());
        if (security == null) {
            return null;
        }

        return new DiscoveredServerSettings(server.getType(), server.getHostname(), server.getPort(), security,
                authType, server.getUsername());
    }

    private static AuthType getAuthType(String authString) {
        if (Utility.isStringEmpty(authString)) {
            return null;
        }
        authString = authString.toUpperCase();
        switch (authString) {
            case "PASSWORD-CLEARTEXT":
                return AuthType.PLAIN;
            case "PASSWORD-ENCRYPTED":
                return AuthType.CRAM_MD5;
            case "OAUTH2":
                return AuthType.XOAUTH2;
        }
        return null;
    }

    private static ConnectionSecurity getConnectionSecurity(String securityString) {
        if (Utility.isStringEmpty(securityString)) {
            return null;
        }
        securityString = securityString.toUpperCase();
        switch (securityString) {
            case "SSL":
                return ConnectionSecurity.SSL_TLS_REQUIRED;
            case "STARTTLS":
                return ConnectionSecurity.STARTTLS_REQUIRED;
            case "PLAIN":
                return ConnectionSecurity.NONE;
        }
        return null;
    }

    private static DiscoveredServerSettings getAutoConfigIncomingSettings(EeloMailAutoConfigResponse response) {
        if (response.getEmailProvider() == null || response.getEmailProvider().getIncomingServer() == null) {
            return null;
        }
        return getAutoConfigSettings(response.getEmailProvider().getIncomingServer());
    }

    private static DiscoveredServerSettings getAutoConfigOutgoingSettings(EeloMailAutoConfigResponse response) {
        if (response.getEmailProvider() == null || response.getEmailProvider().getOutgoingServer() == null) {
            return null;
        }
        return getAutoConfigSettings(response.getEmailProvider().getOutgoingServer());
    }

    private MailAutoConfigDiscovery() {
    }
}
