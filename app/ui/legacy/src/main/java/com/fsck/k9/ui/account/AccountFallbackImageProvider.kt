package com.fsck.k9.ui.account

import android.content.Context
import android.graphics.drawable.Drawable
import android.graphics.drawable.GradientDrawable
import androidx.core.content.ContextCompat
import com.fsck.k9.ui.R
import com.lamounjush.yetanotheraccountchip.ChipDrawableBuilder
import com.lamounjush.yetanotheraccountchip.ChipTextStyle

/**
 * Provides a [Drawable] for the account using the account's color as background color.
 */
class AccountFallbackImageProvider(private val context: Context) {
    fun getDrawable(email: String, color: Int, tag: String?): Drawable {

        //tag == PROFILE MEANS drawer header, textSize should be bigger
        var textSize = 16.0f
        if ("PROFILE" == tag) {
            textSize = 28.0f
        }

        return ChipDrawableBuilder()
            .setBackgroundShape(GradientDrawable.OVAL)
            .setBackgroundColor(color)
            .setTextColor(ContextCompat.getColor(context, R.color.white))
            .setTextSizeInSp(context, textSize)
            .setChipTextStyle(ChipTextStyle.FIRST_CHAR_UPPERCASE)
            .build(email)
    }
}
