package com.fsck.k9.ui.messagelist

import com.google.gson.GsonBuilder
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import org.koin.android.ext.koin.androidContext
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val messageListUiModule = module {
    viewModel { MessageListViewModel(get()) }
    factory { DefaultFolderProvider() }
    factory {
        MessageListLoader(
            preferences = get(),
            localStoreProvider = get(),
            messageListRepository = get(),
            messageHelper = get(),
        )
    }
    single { Moshi.Builder().add(KotlinJsonAdapterFactory()) .build() }
    single { GsonBuilder().registerTypeAdapter(CharSequence::class.java, CharSequenceTypeAdapter()).create() }
    single<EmailCache> { EmailCache(androidContext(), get()) }
    factory {
        MessageListLiveDataFactory(messageListLoader = get(), preferences = get(), messageListRepository = get(), emailCache = get())
    }
    single { SortTypeToastProvider() }
}
