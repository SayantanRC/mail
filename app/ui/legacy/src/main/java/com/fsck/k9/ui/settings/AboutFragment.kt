package com.fsck.k9.ui.settings

import android.content.pm.PackageManager
import android.os.Bundle
import androidx.preference.Preference
import com.fsck.k9.ui.R
import com.takisoft.preferencex.PreferenceFragmentCompat
import timber.log.Timber

class AboutFragment : PreferenceFragmentCompat() {

    override fun onCreatePreferencesFix(savedInstanceState: Bundle?, rootKey: String?) {
        addPreferencesFromResource(R.xml.about_preferences)
        val versionPreference: Preference? = findPreference("build_version")
        versionPreference?.summary = getVersionNumber()
    }

    private fun getVersionNumber(): String {
        return try {
            val context = requireContext()
            val packageInfo = context.packageManager.getPackageInfo(context.packageName, 0)
            packageInfo.versionName
        } catch (e: PackageManager.NameNotFoundException) {
            Timber.e(e, "Error getting PackageInfo")
            "?"
        }
    }
}
