package app.k9mail.feature.account.oauth.domain.usecase

import app.k9mail.core.android.common.accountmanager.AccountManagerHelper
import app.k9mail.feature.account.oauth.domain.AccountOAuthDomainContract.UseCase

internal class CheckIsAccountManagerOpenIdSignIn : UseCase.CheckIsAccountManagerOpenIdSignIn {
    override fun execute(hostname: String): Boolean {
        return AccountManagerHelper.getOpenIdAccountTypeByHostName(hostname) != null
    }
}
